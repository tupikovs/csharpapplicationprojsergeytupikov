﻿namespace QAApplicationProj.Data.Entities
{
    public class Car
    {
        public string Make { get; set; }

        public string Model { get; set; }

        public int Year { get; set; }

        protected bool Equals(Car other) 
        {
            return string.Equals(Make, other.Make) && string.Equals(Model, other.Model) && Year == other.Year;
        }

        public override bool Equals(object obj) 
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            return obj.GetType() == GetType() && Equals((Car) obj);
        }

        public override int GetHashCode()
        {
            unchecked {
                var hashCode = Make?.GetHashCode() ?? 0;
                hashCode = (hashCode*397) ^ (Model?.GetHashCode() ?? 0);
                hashCode = (hashCode*397) ^ Year;
                return hashCode;
            }
        }
    }
}
