﻿using QAApplicationProj.Helpers;
using System;
using System.Collections.Generic;
using Xunit;

namespace QAApplicationProj.Tests.Helpers
{
    public class FareHelperTests
    {
        // tests against ArgumentOutOfRangeException
        public static IEnumerable<object[]> AddFaresDataArgumentOutOfRangeException
        {
            get
            {
                return new[]
                {
                new object[] { decimal.One, decimal.MinusOne },
                new object[] { decimal.MinusOne, decimal.One },
                new object[] { decimal.MinusOne, decimal.MinusOne },
                new object[] { decimal.MinusOne, decimal.Zero },
                new object[] { decimal.Zero, decimal.MinusOne },
                new object[] { decimal.Zero, decimal.MinValue },
                new object[] { decimal.MinValue, decimal.MinusOne },
                new object[] { decimal.MinValue, decimal.MinValue }
            };
            }
        }

        [Theory, MemberData("AddFaresDataArgumentOutOfRangeException")]
        public void AddFares_negative_tests(decimal firstFare, decimal secondFare)
        {
            Assert.Throws<ArgumentOutOfRangeException>(() => FareHelper.AddFares(firstFare, secondFare));
        }


        // tests against OverflowException
        public static IEnumerable<object[]> AddFaresDataOverflowException
        {
            get
            {
                return new[]
                {
                new object[] { decimal.One, decimal.MaxValue },
                new object[] { decimal.MaxValue, decimal.One },
                new object[] { decimal.MaxValue, decimal.MaxValue }
            };
            }
        }

        [Theory, MemberData("AddFaresDataOverflowException")]
        public void AddFaresOverflowExceptionTests(decimal firstFare, decimal secondFare)
        {
            Assert.Throws<OverflowException>(() => FareHelper.AddFares(firstFare, secondFare));
        }


        // positive tests
        public static IEnumerable<object[]> AddFaresDataPositivetests
        {
            get
            {
                return new[]
                {
                new object[] { decimal.Zero, decimal.Zero, decimal.Zero },
                new object[] { decimal.Zero, decimal.One, decimal.One },
                new object[] { decimal.One, decimal.Zero, decimal.One },
                new object[] { decimal.Zero, decimal.MaxValue, decimal.MaxValue },
                new object[] { decimal.MaxValue, decimal.Zero, decimal.MaxValue },
                new object[] { decimal.One, decimal.One, 2m },
                new object[] { (decimal.MaxValue - decimal.One), decimal.One, decimal.MaxValue },
                new object[] { decimal.One, (decimal.MaxValue - decimal.One), decimal.MaxValue },
                new object[] { (decimal.MaxValue - 0.01m), 0.01m, decimal.MaxValue },
                new object[] { 0.01m, (decimal.MaxValue - 0.01m), decimal.MaxValue },
                new object[] { 0, 0.01m, 0.01m },
                new object[] { 0.01m, 0, 0.01m },
                new object[] { 0.01m, 0.01m, 0.02m }
            };
            }
        }

        [Theory, MemberData("AddFaresDataPositivetests")]
        public void AddFares_positive_tests(decimal firstFare, decimal secondFare, decimal expectedTotal)
        {
            Assert.Equal(expectedTotal, FareHelper.AddFares(firstFare, secondFare));
        }
    }
}
