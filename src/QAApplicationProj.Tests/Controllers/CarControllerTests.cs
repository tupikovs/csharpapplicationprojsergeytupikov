﻿using QAApplicationProj.Controllers;
using QAApplicationProj.Data.Entities;
using QAApplicationProj.Data.Repositories.Interfaces;
using QAApplicationProj.Requests;
using QAApplicationProj.Responses;
using System;
using Xunit;

namespace QAApplicationProj.Tests.Controllers
{
    // I created this class based on this thread
    // http://stackoverflow.com/questions/10048260/nosql-how-to-mock-database-for-unit-testing
    //
    public class MockCarRepository : ICarRepository
    {
        public bool Exists(Car car)
        {
            if (ShouldBeException(car))
            {
                throw new Exception();
            }
            return (car.Model == "exists");
        }

        public void Create(Car car)
        {
            if (ShouldBeException(car))
            {
                throw new Exception();
            }
        }

        private bool ShouldBeException(Car car)
        {
            return (car.Make == "exception");
        }
    }

    public class CarControllerTests
    {
        private readonly CarController _controller;
        private readonly CarRequest _request;

        public CarControllerTests()
        {
            _controller = new CarController(new MockCarRepository());
            _request = new CarRequest();
            _request.Year = 2016;
            _request.Make = "foo";
            _request.Model = "bar";
        }

        [Fact]
        public void CreateCar_InvalideRequest()
        {
            var resp = _controller.CreateCar(new CarRequest());
            Assert.IsType<CreateCarResponse>(resp);
            Assert.Equal("The request did not pass validation.", resp.Message);
        }

        [Fact]
        public void CreateCar_CarDoesNotExistNoException()
        {
            _request.Year = 2016;
            _request.Make = "foo";
            _request.Model = "bar";
            var resp = _controller.CreateCar(_request);
            Assert.IsType<CreateCarResponse>(resp);
            Assert.True(resp.WasSuccessful);
        }

        [Fact]
        public void CreateCar_CarExistsNoException()
        {
            _request.Model = "exists";
            _request.Make = "bar";
            var resp = _controller.CreateCar(_request);
            Assert.IsType<CreateCarResponse>(resp);
            Assert.Equal("The requested Car already exists.", resp.Message);
            Assert.False(resp.WasSuccessful);
        }

        [Fact]
        public void CreateCar_CarDoesNotExistException()
        {
            _request.Model = "foo";
            _request.Make = "exception";
            var resp = _controller.CreateCar(_request);
            Assert.IsType<CreateCarResponse>(resp);
            Assert.Equal("An unhandled exception occurred while creating the requested Car.", resp.Message);
            Assert.False(resp.WasSuccessful);
        }

        [Fact]
        public void CreateCar_CarExistsException()
        {
            _request.Model = "exists";
            _request.Make = "exception";
            var resp = _controller.CreateCar(_request);
            Assert.IsType<CreateCarResponse>(resp);
            Assert.Equal("An unhandled exception occurred while creating the requested Car.", resp.Message);
            Assert.False(resp.WasSuccessful);
        }
    }
}
